const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}
// 1.
let userIntersedVideoGames= Object.entries(users).filter((obj)=>{
    obj[1].interests == obj[1].interests.join(" ")
    return obj[1].interests.includes("Video Games")
}).map((obj)=>{
    return obj[0];
})

console.log(userIntersedVideoGames);

//2.

let userStayInGermany = Object.entries(users).filter((obj)=>{
    if(obj[1].nationality == "Germany"){
        return true ;
    }
})
console.log(userStayInGermany);

//3.
    // for Age - 20 >10 
let seniorityLevel = Object.entries(users)
.sort((val1,val2)=>{
   if(val1[1].age > val2[1].age){
    return -1;
   }else{
    return 1 ;
   }
})
console.log(seniorityLevel)

// 4.

let userHaveMasters = Object.entries(users)
.filter((obj)=>{
    if(obj[1].qualification == 'Masters'){
        return true ;
    }
})
console.log(userHaveMasters);